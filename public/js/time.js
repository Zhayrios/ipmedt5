window.onload = () =>{

  const fetchApi = (url) =>{
    return fetch(url).then((data) =>{
      return data.json();
    });
  }

  console.log("boi");

let currentTime = 0;
let myTime = 0;
let counter = 0;
//zorg dat de opgegeven tijd gebruikt wordt
fetchApi("/getTime").then((response) =>{
  myTime = response[0].Eindvandag;
})
//check of de tijd die je hebt ingesteld heb gelijk is aan de daadwerkelijke tijd
//zoja, geef dan een popup
//doe dit elke seconde (met de setTimeout functie) zodat je constant blijft checken
const checkTime = () =>{
  if (counter == 0) {
    if (myTime == currentTime) {
      toastr.info("Het einde van uw dag naderd, check of er nog fruit in uw machine ligt");
    }
  }

  counter = 1;

  if (myTime != currentTime) {
    counter = 0;
  }
  console.log(counter);
  setTimeout(checkTime, 1000);
}

//haal de daadwerkelijke tijd op en sla het op in een variabele
//doe dit elke seconde voor een realistische tijd
const getTime = () => {

  var today = new Date();
  if (today.getMinutes() < 10) {
   currentTime = today.getHours() +":" + 0 + today.getMinutes();
  }else {
   currentTime = today.getHours() +":" + today.getMinutes();
  }
  setTimeout(getTime, 1000);
}

fetchApi('/getTimeData').then((response) =>{
  const checkIfZero = () =>{
    for (let i = 0; i < response.length; i++) {
      if (response[i].dagscore == 0 && myTime == currentTime) {
        toastr.info(response[i].gebruikersnaam + ' heeft geen fruit op vandaag');
      }
    }
  }
checkIfZero();
});

getTime();
checkTime();

}

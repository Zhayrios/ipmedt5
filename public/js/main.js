window.onload = () =>{

  //haal het canvas op uit het blade bestand
  const ctx = document.getElementById("js--canvasDag").getContext('2d');
  const ctx2 = document.getElementById("js--canvasWeek").getContext('2d');
  const ctx3 = document.getElementById("js--canvasMaand").getContext('2d');
  const toAlleScores = document.getElementById('js--toAlleScores');
  const toDagScores = document.getElementById('js--toDagScores');
  const toWeekScores = document.getElementById('js--toWeekScores');
  const toMaandScores = document.getElementById('js--toMaandScores');
  const dagGrafiek = document.getElementById('js--dagGrafiek');
  const weekGrafiek = document.getElementById('js--weekGrafiek');
  const maandGrafiek = document.getElementById('js--maandGrafiek');

  Chart.defaults.global.defaultFontColor = "white";

  //haal data op van
  const fetchApi = (url) =>{
    return fetch(url).then((data) =>{
      return data.json();
    });
  }
  
  //pak de data die meegegeven wordt op pad getData
  fetchApi("/getData").then((response) =>{
    let users = [];
    let dag = [];
    let week = [];
    let maand = [];

    //pak alle items uit de response mee en vul de array's met de data uit de response
    for (let i = 0; i < response.length; i++) {
      users.push(response[i].gebruikersnaam);
      dag.push(response[i].dagscore);
      week.push(response[i].weekscore);
      maand.push(response[i].maandscore);
    }
    //maak de chart aan
    let dagChart = new Chart(ctx, {
      type: 'bar',
      data:{
        //geef de labels de data van de users hun gebruikersnaam mee
        labels: users,
        datasets: [{
          label: 'Aantal stukken fruit per persoon',
          //stop in de data het getal van de dagscore
          data: dag,
          backgroundColor: "rgb(51, 51, 51)",
        }]
      },
      options: {
        maintainAscpectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    let weekChart = new Chart(ctx2, {
      type: 'bar',
      data:{
        //geef de labels de data van de users hun gebruikersnaam mee
        labels: users,
        datasets: [{
          label: 'Aantal stukken fruit per persoon',
          //stop in de data het getal van de dagscore
          data: week,
          backgroundColor: "rgb(51, 51, 51)"
        }]
      },
      options: {
        maintainAscpectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    let maandChart = new Chart(ctx3, {
      type: 'bar',
      data:{
        //geef de labels de data van de users hun gebruikersnaam mee
        labels: users,
        datasets: [{
          label: 'Aantal stukken fruit per persoon',
          //stop in de data het getal van de dagscore
          data: maand,
          backgroundColor: "rgb(51, 51, 51)"
        }]
      },
      options: {
        maintainAscpectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  })

  //buttons om verschillende grafieken te laten zien
  toAlleScores.onclick = () => {
    dagGrafiek.style.display = "block";
    weekGrafiek.style.display = "block";
    maandGrafiek.style.display = "block";
  }

  toDagScores.onclick = () => {
    dagGrafiek.style.display = "block";
    weekGrafiek.style.display = "none";
    maandGrafiek.style.display = "none";
  }

  toWeekScores.onclick = () => {
    dagGrafiek.style.display = "none";
    weekGrafiek.style.display = "block";
    maandGrafiek.style.display = "none";
  }

  toMaandScores.onclick = () => {
    dagGrafiek.style.display = "none";
    weekGrafiek.style.display = "none";
    maandGrafiek.style.display = "block";
  }
}

const vraag = document.getElementsByClassName('vraag-titel');
const antwoord = document.getElementsByClassName('antwoord');
const uitklappen = document.getElementsByClassName('uitklappen');
const badge = document.getElementsByClassName('badge');
const close_popup = document.getElementById('close');
const canvas = document.getElementById('js--canvas');


const antwoordUitklappen = () =>{
    if (antwoord[0].style.display === "block") {
      antwoord[0].style.display = "none";
      vraag[0].style.fontWeight = "normal"
      uitklappen[0].src = "/img/arrow-down.png";
    }
    else {
      antwoord[0].style.display = "block";
      vraag[0].style.fontWeight = "bold"
      uitklappen[0].src = "/img/arrow-up.png";
  }
}

const antwoordUitklappen1 = () =>{
    if (antwoord[1].style.display === "block") {
      antwoord[1].style.display = "none";
      vraag[1].style.fontWeight = "normal"
      uitklappen[1].src = "/img/arrow-down.png";
    }
    else {
      antwoord[1].style.display = "block";
      vraag[1].style.fontWeight = "bold"
      uitklappen[1].src = "/img/arrow-up.png";
  }
}

const antwoordUitklappen2 = () =>{
    if (antwoord[2].style.display === "block") {
      antwoord[2].style.display = "none";
      vraag[2].style.fontWeight = "normal"
      uitklappen[2].src = "/img/arrow-down.png";
    }
    else {
      antwoord[2].style.display = "block";
      vraag[2].style.fontWeight = "bold"
      uitklappen[2].src = "/img/arrow-up.png";
  }
}

const antwoordUitklappen3 = () =>{
    if (antwoord[3].style.display === "block") {
      antwoord[3].style.display = "none";
      vraag[3].style.fontWeight = "normal"
      uitklappen[3].src = "/img/arrow-down.png";
    }
    else {
      antwoord[3].style.display = "block";
      vraag[3].style.fontWeight = "bold"
      uitklappen[3].src = "/img/arrow-up.png";
  }
}
// Code hieronder is testcode, dit wordt vervangt met database
let aantal_fruit = loggedUser.stukkenfruitgegeten;
let aantal_beloningen = loggedUser.stukkenfruitgegeten/100;
let aantal_streak = 0;
let aantal_dagen_ingebruik = loggedUser.aantaldagengebruik;
let leader = 0;
let leaderNaam = hoogsteScore.gebruikersnaam;//haalt de naam van de eerste persoon in leaderboard
let verwijderd = 0;
let dagwaarde = loggedUser.aantaldagengebruik;
let loggeduserNaam = scores[0].gebruikersnaam;//haalt de naam van de logged gebruiker op
let badgeWaarde = 0;

//functie moet aangeroepen worden in de file
const CheckBadge = () =>{
  aantal_streak += 1;
  verwijderd += 2;
  checkLeader();
  badge_fruiteter();
  badge_verzamelaar();
  badge_streak();
  badge_winnaar();
  badge_opruimer();
  badge_oudje();
}

const checkLeader = () =>{
    if (loggeduserNaam == leaderNaam){
        leader += 1;
        console.log('je bent eerste. Je bent al ' + leader +" geweest");
    } else{
        console.log("je bent geen eerste geworden");
    }
}

// Einde testcode

const badge_fruiteter = () =>{
  if(aantal_fruit >= 1){   //aantal_fruit veranderen naar $fruitaantal
    badge[0].src = "/img/fruiteter_brons.png"; //badge img veranderen naar brons
    badgeWaarde = 1;
    badge_popup();
  }
  else if (aantal_fruit == 15) {
    badge[0].src = "/img/fruiteter_zilver.png"; //badge img veranderen naar zilver
    badgeWaarde = 1;
    badge_popup();
  }
  else if (aantal_fruit == 50) {
    badge[0].src = "/img/fruiteter_goud.png"; //badge img veranderen naar gold
    badgeWaarde = 1;
    badge_popup();
  }
}

const badge_verzamelaar = () =>{
  if(aantal_beloningen == 1){   //aantal_beloningen veranderen naar $beloningaantal
    badge[1].src = "/img/verzamelaar_brons.png"; //badge img veranderen naar brons
    badgeWaarde = 2;
    badge_popup();
  }
  else if (aantal_beloningen == 5) {
    badge[1].src = "/img/verzamelaar_zilver.png"; //badge img veranderen naar zilver
    badgeWaarde = 2;
    badge_popup();
  }
  else if (aantal_beloningen == 25) {
    badge[1].src = "/img/verzamelaar_goud.png";  //badge img veranderen naar gold
    badgeWaarde = 2;
    badge_popup();
  }
}

const badge_streak = () =>{
  if(aantal_streak == 2){   //streak_beloningen veranderen naar $streakaantal
    badge[2].src = "/img/streak_brons.png";  //badge img veranderen naar brons
    badgeWaarde = 3;
    badge_popup();
  }
  else if (aantal_streak == 10) {
    badge[2].src = "/img/streak_zilver.png";   //badge img veranderen naar zilver
    badgeWaarde = 3;
    badge_popup();
  }
  else if (aantal_streak == 30) {
    badge[2].src = "/img/streak_goud.png";   //badge img veranderen naar gold
    badgeWaarde = 3;
    badge_popup();
  }
}

const badge_winnaar = () =>{
  //leaderboard nummer1 pakken bij eind van de week wanneer leaderboard refreshed wordt.
  console.log(leader);
   if(leader == 1){badge[3].src = "/img/kampioen_brons.png";badgeWaarde = 4;badge_popup();}
   else if(leader == 3){badge[3].src = "/img/kampioen_zilver.png";badgeWaarde = 4;badge_popup();}
   else if(leader == 10){badge[3].src = "/img/kampioen_goud.png";badgeWaarde = 4;badge_popup();}
}

const badge_opruimer = () =>{
  //Waarde aantal verwijderde berichten pakken
   if(verwijderd == 2){badge[4].src = "/img/opruimer_brons.png";badgeWaarde = 5;badge_popup();}
   else if(verwijderd == 10){badge[4].src = "/img/opruimer_zilver.png";badgeWaarde = 5;badge_popup();}
   else if(verwijderd == 30){badge[4].src = "/img/opruimer_goud.png";badgeWaarde = 5;badge_popup();}
}

const badge_oudje = () =>{
  // Datumwaarde bijhouden vanaf gemaakte account. dagwaarde ervan pakken
   if(dagwaarde == 1){badge[5].src = "/img/oudje_brons.png";badgeWaarde = 6;badge_popup();}
   else if(dagwaarde == 7){badge[5].src = "/img/oudje_zilver.png";badgeWaarde = 6;badge_popup();}
   else if(dagwaarde == 30){badge[5].src = "/img/oudje_goud.png";badgeWaarde = 6;badge_popup();}
}

//laat popup zien wanneer je een badge krijgt
const badge_popup = () =>{
  popup.style.display = "block";
}

//popup gaat weg
const close = document.getElementById("close");
close.onclick = function(){
  popup.style.display = "none";
  badgeAnimation();
}

//popup gaat weg
window.onclick = function(event) {
  const popup = document.getElementById('popup');
  if (event.target == popup) {
    popup.style.display = "none";
    badgeAnimation();
  }
}

//badge animaties wanneer je een badge verdient
const badgeAnimation = () =>{
  console.log(`badge ${badgeWaarde}`)
  if (badgeWaarde == 1){
  const badge = gsap.timeline();
    badge.to('.badge1', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge1', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge1', { duration: 0, rotation: 0});
    badgeWaarde -= 1;
  }
  if (badgeWaarde == 2){
  const badge = gsap.timeline();
    badge.to('.badge2', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge2', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge2', { duration: 0, rotation: 0});
    badgeWaarde -= 2;
  }
  if (badgeWaarde == 3){
  const badge = gsap.timeline();
    badge.to('.badge3', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge3', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge3', { duration: 0, rotation: 0});
    badgeWaarde -= 3;
  }
  if (badgeWaarde == 4){
  const badge = gsap.timeline();
    badge.to('.badge4', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge4', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge4', { duration: 0, rotation: 0});
    badgeWaarde -= 4;
  }
  if (badgeWaarde == 5){
  const badge = gsap.timeline();
    badge.to('.badge5', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge5', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge5', { duration: 0, rotation: 0});
    badgeWaarde -= 5;
  }
  if (badgeWaarde == 6){
  const badge = gsap.timeline();
    badge.to('.badge6', { opacity: 0.6, duration: 0.5, rotation: 180});
    badge.to('.badge6', { opacity: 1, duration: 0.5, width:75, height: 75, rotation: 360});
    badge.to('.badge6', { duration: 0, rotation: 0});
    badgeWaarde -= 6;
  }

}
function goBack() {
  window.history.back();
}

//gsap animatie informatiescherm & hintscherm waarbij vraagcontainers naar beneden droppen.
if(window.location.pathname == '/informatiescherm' || window.location.pathname == '/hintscherm'){
const tl2 = gsap.timeline();
  tl2.from(".v1",{duration:2, y: -650, ease: "elastic"});
  tl2.from([".v2",".v3", ".v4"],{duration:2, y: -700, ease: "elastic", delay: -1.98});
  // tl2.from(".v3",{duration:2, y: -750, ease: "elastic", delay: -1.98});
  // tl2.from(".v4",{duration:2, y: -800, ease: "elastic", delay: -1.98});
}

//gsap animatie leaderboardscherm waarbij een random weetje voorbij animeert elke keer dat je de pagina bezoekt.
if(window.location.pathname == '/leaderboard'){
  const weetjes = document.getElementById('weetjes');
  const weetjesLijst = ["komkommer fruit is", "een mandarijn uit 9/10 stukjes bestaat", "limoen het zuurste fruitsoort is", "dadels het zoetste fruitsoort is", "je dagelijks ongeveer 2 porties fruit moet eten", "niet alleen te weinig, maar ook teveel fruit ongezond is", "appels het meest gegeten fruitsoort is in Nederland", "tomaat fruit is"];
  const tl = gsap.timeline();
    tl.from(".animatie", { duration: 2.5, x: -300, ease: "elastic"});
    tl.to(".animatie", {delay: 5, duration: 2, x: 3000, ease: "power1.in"});
    weetjes.innerHTML = weetjesLijst[Math.floor(Math.random() * weetjesLijst.length)];
}

if(window.location.pathname == '/user'){
  if (window.matchMedia("(min-width: 800px)").matches) {
    gsap.from('.cf1',{duration:1.5, y: -1000, ease: "elastic"});
    gsap.from('.cf2',{duration:1.5, y: -1000, ease: "elastic", delay: 0.05});
  }
  else{
    gsap.from('.cf1',{duration:1.5, y: -500, ease: "elastic"});
    gsap.from('.cf2',{duration:1.5, y: -500, ease: "elastic", delay: 0.02});
  }
}



//Nog in testfase
if(window.location.pathname == '/user'){
  let button = document.getElementsByClassName("button")
  button[0].addEventListener("mouseenter", doCoolStuff);
  button[0].addEventListener("mouseleave", doCoolStuff);
  button[1].addEventListener("mouseenter", doCoolStuff1);
  button[1].addEventListener("mouseleave", doCoolStuff1);
  button[2].addEventListener("mouseenter", doCoolStuff2);
  button[2].addEventListener("mouseleave", doCoolStuff2);

  const tl = new TimelineMax();
  tl.to(button[0], { duration: 0.5, width:"-=20", height: "+=2", ease:"bounce"});
  const tl1 = new TimelineMax();
  tl1.to(button[1], {duration: 0.4, width:"-=20", height: "+=2", ease:"bounce"});
  const tl2 = new TimelineMax();
  tl2.to(button[2], {duration: 0.15, width:"-=10", height: "+=2", ease:"normal"});
  //tl2.to(".test", {backgroundColor:"blue"});
  function doCoolStuff() {
      tl.reversed(!tl.reversed());
  }
  function doCoolStuff1() {
      tl1.reversed(!tl1.reversed());
  }
  function doCoolStuff2() {
      tl2.reversed(!tl2.reversed());
  }
}




























//e

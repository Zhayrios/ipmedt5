<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leaderboard extends Model
{
    protected $table = "scores";

    protected $fillable = ['gebruikersnaam', 'dagscore','weekscore', 'maandscore','jaarscore','beloningscore','streak'];

    public function leaderboard()
    {
        return $this->hasMany('App\User', 'gebruikersnaam', 'gebruikersnaam');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fingerprint extends Model
{
    public $timestamps = false;
        protected $fillable = [
            'gebruikersnaam'
        ];

    public function user(){
        return $this->belongsTo('App\User','name','name');
    }
}

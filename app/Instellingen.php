<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instellingen extends Model
{
    protected $table = "instellingen";
    public $timestamps = false;
}

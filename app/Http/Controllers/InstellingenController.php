<?php

namespace App\Http\Controllers;

use App\Instellingen;
use App\Scores;
use Illuminate\Http\Request;

class InstellingenController extends Controller
{
    public function show(){
    $instellingen = Instellingen::all();
    return view('instellingen')->with('instellingen',$instellingen);
    }

    public function update(){
      return view('updateInstellingen')->with('instellingen', Instellingen::all());
    }

    public function patch(Request $request){
      try {
        Instellingen::query()->update([
          'minaantalfruit' => $request->input('minaantalfruit'),
          'maxaantalfruit' => $request->input('maxaantalfruit'),
          'Eindvandag' => $request->input('Eindvandag')
        ]);
        toastr()->success('Instellingen zijn geüpdatet!');
        return redirect("/instellingen");

      } catch (Exception $e) {
        toastr()->error('Er ging iets fout en er is niks opgeslagen!');
        return redirect("/instellingen/update");
      }
    }

    public function getTime(){
      return Instellingen::all();
    }

    public function getTimeData(){
      return Scores::all();
    }
}

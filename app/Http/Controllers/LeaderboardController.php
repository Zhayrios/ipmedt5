<?php

namespace App\Http\Controllers;

use App\Leaderboard;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User as AppUser;
use App\Scores;
use App\Gezin;
use App\users;
use Illuminate\Http\Request;

class LeaderboardController extends Controller
{
    public function show(){

        $gezin = DB::table('scores')
        ->join('users', 'scores.gebruikersnaam','=','users.name')
        ->Select('users.name','scores.dagscore','scores.streak','users.groepcode')
        ->where('users.groepcode','=',Auth::user()->groepcode)->orderBy('dagscore','desc')->get();



      return view('leaderboard')->with('gezin',$gezin);
    }
}

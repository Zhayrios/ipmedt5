<?php

namespace App\Http\Controllers;
use App\User as AppUser;
use App\Scores;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function show(){


    $users = AppUser::all();
    $scores = Scores::where('gebruikersnaam','=', Auth::user()->name)->get();
    $hoogsteScore = Scores::orderBy('dagscore','desc')->first();

    return view('account',compact(['scores','users','hoogsteScore']))->with('user',$users)->with('scores',$scores)->with('hoogsteScore'.$hoogsteScore);
    }


}

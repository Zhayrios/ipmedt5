<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class GroepcodeController extends Controller
{
    public function show(){
        $groepcode = Auth::user()->groepcode;
        return view('groepcode')->with('groepcode',$groepcode);
        }

        public function update(){
          return view('updateGroepcodeInstellingen')->with('groepcode', Auth::user()->groepcode);
        }

        public function patch(Request $request){
            try {
              User::where('name','=',Auth::user()->name)->update([
                  'groepcode' => $request->input('groepcode')
              ]);
              toastr()->success('Groepcode instellingen zijn geüpdatet!');
              return redirect("/groepcode");

            } catch (Exception $e) {
              toastr()->error('Er ging iets fout en er is niks opgeslagen!');
              return redirect("/groepcode/update");
            }
          }
}

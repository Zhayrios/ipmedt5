<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Scores;
use exception;

class GrafiekenController extends Controller
{
    public function show(){
      return view('graphs')->with('users',Scores::all());
    }

    public function getData(){
        return Scores::all();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public function getUsers(){
        return $this->hasMany('App\User','gebruikersnaam','gebruikersnaam');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
    protected $table = 'user';
    public $increments = false;

    public function getFingerprint(){
        return $this->hasOne('App\Fingerprint','gebruikersnaam','gebruikersnaam');
    }

    public function getScores(){
        return $this->hasOne('App\scores','gebruikersnaam','gebruikersnaam');
    }

    public function getGroep(){
        return $this->hasOne('App\gezin','groepscode','groepscode');
    }

}

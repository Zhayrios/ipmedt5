import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    user="tijn",
    passwd="password",
    database="ipmedt5"
)

mycursor = mydb.cursor()

mycursor.execute("UPDATE scores SET dagscore = dagscore + 1")
mycursor.execute("UPDATE scores SET weekscore = weekscore + 1")
mycursor.execute("UPDATE scores SET maandscore = maandscore + 1")
mycursor.execute("UPDATE scores SET weekscore = weekscore + 1")
mycursor.execute("UPDATE users SET stukkenfruitgegeten = stukkenfruitgegeten + 1")
mydb.commit()

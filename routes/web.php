<?php

use App\Instellingen;
use App\User;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@show');

Route::get('/instellingen','InstellingenController@show')->middleware('auth');
Route::get('/instellingen/update', 'InstellingenController@update')->middleware('auth');
Route::put('/instellingen/patch', 'InstellingenController@patch')->middleware('auth');
Route::get('/getTime', 'InstellingenController@getTime')->middleware('auth');
Route::get('/getTimeData', 'InstellingenController@getTimeData')->middleware('auth');

Route::get('/user','UserController@show')->middleware('auth');

Route::get('/leaderboard', 'LeaderboardController@show')->middleware('auth');

Route::get('/groepcode', 'GroepcodeController@show')->middleware('auth');
Route::get('/groepcode/update', 'GroepcodeController@update')->middleware('auth');
Route::put('/groepcode/patch', 'GroepcodeController@patch')->middleware('auth');

Route::get('/informatiescherm', function() {
  return view('informatiescherm');
});

Route::get('/hintscherm', function() {
  return view('hintscherm');
});

Route::get('/badgescherm', function() {
  return view('badgescherm');
});

//route naar de grafieken
Route::get('/graphs', 'GrafiekenController@show')->middleware('auth');
Route::get('/getData', 'GrafiekenController@getData')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::fallback(function() {
    return view('fallback');
});

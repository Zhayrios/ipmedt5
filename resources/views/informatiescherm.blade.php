@extends('layout.layout')


@section('content')
  <nav>
    <ul class="navigatie-top">
      <li> <a href="/user"> <img src="/img/previous.png" class="navigation-icon"> </a> </img> </li>
      <li><a href="/hintscherm"> <img src="/img/tips.png" class="navigation-icon"> </a> </li>
    </ul>
  </nav>

  <div class="main-container">
  <article>
    <h1> Informatie </h1>

    <div class ="container-vraag v1" onclick="antwoordUitklappen()">
      <div class="vraag">
        <img src="/img/arrow-down.png" class ="uitklappen" > </img>
        <h2 class ="vraag-titel"> Hoe pak je een stuk fruit? </h2>
      </div>

      <div class="antwoord">
        <p> 1. Scan je vinger op het apparaat. </p>
        <p> 2. Druk op de fruitknop. </p>
        <p> 3. Wacht tot het fruit eruit komt. </p>
      </div>
    </div>

    <div class ="container-vraag v2" onclick="antwoordUitklappen1()">
      <div class="vraag">
        <img src="/img/arrow-down.png" class ="uitklappen"> </img>
        <h2 class ="vraag-titel"> Hoe pak je een beloning? </h2>
      </div>
      <div class="antwoord">
        <p> 1. Scan je vinger op het apparaat. </p>
        <p> 2. Druk op de beloning knop. </p>
        <p> 3. Wacht tot de beloning eruit komt. </p>
      </div>
    </div>

    <div class ="container-vraag v3" onclick="antwoordUitklappen2()">
      <div class="vraag">
        <img src="/img/arrow-down.png" class ="uitklappen"> </img>
        <h2 class ="vraag-titel"> Hoe kies ik een ander stuk fruit? </h2>
      </div>
      <div class="antwoord">
        <p> 1. Scan je vinger op het apparaat. </p>
        <p> 2. Druk op de linker of rechterknop voor een andere keuze. </p>
        <p> 3. Druk op de fruitknop. </p>
        <p> 4. Wacht tot de fruit eruit komt. </p>
      </div>
    </div>

    <div class ="container-vraag v4" onclick="antwoordUitklappen3()">

      <div class="vraag">
        <img src="/img/arrow-down.png" class ="uitklappen"> </img>
        <h2 class ="vraag-titel"> Hoe vul ik bij? </h2>
      </div>
      <div class="antwoord">
        <p> 1. Open de slot aan de achterkant met de sleutel. </p>
        <p> 2. Zet het zakje met fruit in de fruitcontainer.</p>
        <p> 3. Zet het zakje met een beloning in de beloningcontainer. </p>
        <p> 4. Doe het weer op slot. </p>
      </div>
    </div>
  </article>
  <div class="main-container">
<!-- gsap werkt niet als de script tag boven de aangegeven animatie komt -->
<script type="text/javascript" src="{{URL::asset('js/animatie.js')}}"></script>
@endsection

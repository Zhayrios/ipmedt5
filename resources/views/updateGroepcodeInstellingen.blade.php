@extends('layout.layoutNav')


@section('content')


<nav>
  <ul class="navigatie-top">
    <li> <a href="/groepcode"> <img src="/img/previous.png" class="navigation-icon"> </a> </img> </li>
  </ul>
</nav>
  <div class="main-container">
  <h1>Groepcode Instellingen</h1>
      <form class="form" action="/groepcode/patch" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <label for="groepcode">Je groepcode is: </label>
        <input type="string" name="groepcode" id="groepcode" value="{{$groepcode}}"> <br>

        <button type="submit" class="button" name="button">Opslaan</button>
      </form>
  </div>


@endsection

@extends('layout.layoutNav')


@section('content')
<div class="main-container">

  <h1>Leaderboard</h1>

  <ul class="leaderboard">
    <li class="leaderboard-item">
      <p class="leaderboard-item-naam"> <img src="img/account.png" alt="Account" title="Namen"> </p>
      <p class="leaderboard-item-score"> <img src="img/fruit.png" alt="Aantal fruit" title="Aantal fruit"> </p>
      <p class="leaderboard-item-streak"> <img src="img/streak.png" alt="Streak" title="Streak"> </p>
    </li>
  @foreach ($gezin as $lid)
    <li class="leaderboard-item">
      <p class="leaderboard-item-naam"> {{ $lid->name }} </p>
      <p class="leaderboard-item-score">{{ $lid->dagscore }}</p>
      <p class="leaderboard-item-streak">{{ $lid->streak }}</p>
    </li>
  @endforeach

  </ul>
  <div>
  </div>
  <div class="animatie">
    <img class = 'animatie-foto' src="/img/sinaasappel-gezicht.png" alt="sinaasappel">
    <p class = "animatie-tekst">Hey {{ Auth::user()->name }}! Wist je dat <span id="weetjes"></span>?</p>
  </div>
</div>
<script type="text/javascript" src="{{URL::asset('js/animatie.js')}}"></script>


@endsection

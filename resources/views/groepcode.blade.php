@extends('layout.layoutNav')


@section('content')


<nav>
  <ul class="navigatie-top">
    <li> <a href="/user"> <img src="/img/previous.png" class="navigation-icon"> </a> </img> </li>
  </ul>
</nav>

<div class="main-container">
  <h1>Groepcode Instellen</h1>
  <div class='flex-row'>
      <div class='p-2 bd-highlight'>
            <p>De groepscode die je momenteel hebt is "{{$groepcode}}"</p>
      </div>
      <div class='p-2 bd-highlight'>
          <button type='button'class='button' onclick="window.location.href = '/groepcode/update'">Pas de Groepcode instellingen aan</button>
      </div>
  </div>
</div>


@endsection

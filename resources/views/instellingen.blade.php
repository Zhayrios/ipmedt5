@extends('layout.layout')


@section('content')

<nav>
  <ul class="navigatie-top">
    <li> <a href="/user"> <img src="/img/previous.png" class="navigation-icon"> </a> </img> </li>
  </ul>
</nav>

<div class="main-container">
  <h1>Instellingen</h1>
  <div class='flex-row'>
      <div class='p-2 bd-highlight'>
          <p>Het minimaal aantal stukken fruit voor een beloning: <b> {{ $instellingen->first()->maxaantalfruit }} </b></p>
          <p>Het maximaal aantal stukken fruit per dag: <b> {{ $instellingen->first()->minaantalfruit }} </b> </p>
          <p>De tijd voor herinneringsnotificaties: <b> {{ $instellingen->first()->Eindvandag }} </b> </p>
      </div>
      <div class='p-2 bd-highlight'>
          <button type='button'class='button' onclick="window.location.href = '/instellingen/update'">Pas instellingen aan</button>
      </div>
  </div>
</div>

@endsection

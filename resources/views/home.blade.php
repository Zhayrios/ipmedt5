@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Vingerafruk verifiëren</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Zet je vinger op de vingerprintsensor tot het vinkje hieronder goud is.
                    <a href="/groepcode"><button type="submit" class="button" name="button">Overslaan</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

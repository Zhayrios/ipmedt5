@extends('layout.layout')


@section('content')
  <nav>
    <ul class="navigatie-top">
      <li> <a href="/informatiescherm"> <img src="/img/previous.png" class="navigation-icon"> </img> </a> </li>
    </ul>
  </nav>

  <div class="main-container">
    <article>

      <h1> Tips </h1>
      <div class ="container-vraag v1" onclick="antwoordUitklappen()">
        <div class="vraag">
          <img src="/img/arrow-down.png" class ="uitklappen"> </img>
          <h2 class ="vraag-titel"> Hoe kan ik als ouder mijn kind stimuleren om meer fruit te eten? </h2>
        </div>
        <div class="antwoord">
          <p> 1. Zorg ervoor dat je verschillende soorten fruit in een zakje doet, zodat het gevarieërd is. </p>
          <p> 2. Zorg ervoor dat er 2 verschillende zakjes met verschillende fruit is, zodat iemand kan kiezen welke die wilt. </p>
          <p> 3. Zet geen fruit dat niemand lekker vindt. </p>
        </div>
      </div>

      <div class ="container-vraag v2" onclick="antwoordUitklappen1()">
        <div class="vraag">
          <img src="/img/arrow-down.png" class ="uitklappen"> </img>
          <h2 class ="vraag-titel"> Hoe kies ik een juiste beloning? </h2>
        </div>
        <div class="antwoord">
          <p> 1. Vraag iedereen wat voor soort beloning die leuk zou vinden. </p>
          <p> 2. Kies een beloning waarvan je weet dat diegene het wilt. </p>
          <p> 3. Wees creatief en verzin zelf een beloning! </p>
        </div>
      </div>

      <div class ="container-vraag v3" onclick="antwoordUitklappen2()">
        <div class="vraag">
          <img src="/img/arrow-down.png" class ="uitklappen"> </img>
          <h2 class ="vraag-titel"> Kom je er toch niet uit? Je kunt denken aan het volgende: </h2>
        </div>
        <div class="antwoord">
          <p> - Lekkernijen, zoals koekjes, snoep, chocola. </p>
          <p> - Speelgoed. Je kunt bijvoorbeeld op een papiertje noteren dat diegene mag kiezen welke speelgoed die wilt met een specifieke waarde. </p>
          <p> - Computerspel. Veel kinderen zijn dol op computerspellen en willen graag wat geld besteden hieraan. Geef bijvoorbeeld een fortniteskin of een nieuwe wapen als beloning. </p>
          <p> - Restaurant. Je kunt een dagje uit eten met bijvoorbeeld je echtgenoot. </p>
          <p> - Donatie. Een donatie aan een goed doel naar keuze is ook een liefdadige manier van beloning, maar dan voor iemand anders die het hard nodig heeft.</p>
          <p> - Geld. Je kunt ook simpelweg wat geld in het zakje doen. </p>
        </div>
      </div>

    </article>
  <div class="main-container">
  <!-- gsap werkt niet als de script tag boven de aangegeven animatie komt -->
  <script type="text/javascript" src="{{URL::asset('js/animatie.js')}}"></script>
@endsection

@extends('layout.layoutNav')


@section('content')

<nav>
  <ul class="navigatie-top">
    <li> <a href="/instellingen"> <img src="/img/instellingen.png" class="navigation-icon"> </a> </li>
    <li><a href="/informatiescherm"> <img src="/img/informatie.png" class="navigation-icon"> </a> </li>
  </ul>
</nav>

  <div class="container-flex-main">
    <h1>Mijn account</h1>
  </div>

  <div class="container-flex-main">
      {{-- Informatie over de logged user word hier alleen weergegeven --}}
    <div class ="container-flex cf1 container-account">
          <p>Gebruikersnaam: {{ Auth::user()->name }}</p>
          <p>Je hebt {{ Auth::user()->stukkenfruitgegeten }} stukken fruit gegeten</p>
          <p>Je gebruikt deze app al  {{ Auth::user()->aantaldagengebruik }} dagen</p>

          @foreach ($scores as $item)
              <p>Je hebt een streak van {{ $item->streak }}</p>
          @endforeach
      <div class="button-flex">
        <button class="button" type="button" name="button" value="fruit" onclick="CheckBadge()">Badge Check</button>
        <button class="button">Krijg beloning</button>
      </div>
    </div>

    <div class ="container-flex cf2">
      <div class="container" id="container">
        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge1">
          <p>Fruiteter</p>
        </div>

        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge2">
          <p>Verzamelaar</p>
        </div>

        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge3">
          <p>Vurig</p>
        </div>

        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge4">
          <p>Winnaar</p>
        </div>

        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge5">
          <p>Opruimer</p>
        </div>

        <div class="container_badge">
          <image src="/img/locked.png" class="badge badge6">
          <p>Oudje</p>
        </div>
      </div>
    </div>
      <div class="popup" id="popup">
        <div class="popup_content">
          <span class="close" id="close">&times;</span>
          <p>Je hebt een badge gekregen! Bekijk hem in mijn account.</p><br>
          <a class ="button2" href="/user">Mijn account</a>
        </div>
      </div>


    </div>
    {{-- via de script tag in de HTML kan je blade syntax gebruiken om data te halen en te gerbuiken in js --}}
    <script type="text/javascript">
    //maak de variablen aan die in de side.js file worden gebruikt
        let scores = {!! json_encode($scores->toArray()) !!};
        let hoogsteScore = {!! json_encode($hoogsteScore->toArray()) !!};
        let users = {!! json_encode($users->toArray()) !!};
        let loggedUser = {!! json_encode(Auth::user()->toArray()) !!};

    </script>
    <script type="text/javascript" src="{{URL::asset('js/side.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/animatie.js')}}"></script>

@endsection

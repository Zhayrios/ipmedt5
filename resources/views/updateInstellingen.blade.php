@extends('layout.layout')


@section('content')

<nav>
  <ul class="navigatie-top">
    <li> <a href="/instellingen"> <img src="/img/previous.png" class="navigation-icon"> </a> </img> </li>
  </ul>
</nav>

<div class="main-container">
<h1>Instellingen</h1>
    <form class="form" action="/instellingen/patch" method="POST">
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <label for="minaantalfruit">Het minimaal aantal stukken fruit voor een beloning:</label>
      <input type="number" name="minaantalfruit" id="minaantalfruit" value="{{$instellingen->first()->minaantalfruit}}"> <br>
      <label for="maxaantalfruit">Het maximaal aantal stukken fruit per dag:</label>
      <input type="number" name="maxaantalfruit" id="maxaantalfruit" value="{{$instellingen->first()->maxaantalfruit}}"> <br>
      <label for="Eindvandag">De tijd voor herinneringsnotificaties:</label>
      <input type="text" name="Eindvandag" id="Eindvandag" value="{{$instellingen->first()->Eindvandag}}"><br>
      <button type="submit" class="button" name="button">Opslaan</button>
    </form>
</div>
@endsection

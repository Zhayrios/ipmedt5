@extends('layout.layoutNav')

@section('mainHead')
<script type="text/javascript" src="{{URL::asset('js/main.js')}}"></script>
@endsection

@section('content')
<div class="main-container">
  <h1>Statistieken</h1>

    <div class="grafiek-knoppen">
      <h2> Filteren </h2>
      <input type="radio" id="js--toAlleScores" name="button" value="alles" checked>
      <label for="alles">Alle scores</label><br>
      <input type="radio" id="js--toDagScores" name="button" value="dag">
      <label for="dag">Dag scores</label><br>
      <input type="radio" id="js--toWeekScores" name="button" value="week">
      <label for="week">Week scores</label><br>
      <input type="radio" id="js--toMaandScores" name="button" value="maand">
      <label for="maand">Maand scores</label>
    </div>

    <div class="dagGrafiek grafiek" id="js--dagGrafiek" >
      <div class="graphContainer">
          <canvas id="js--canvasDag"></canvas>
      </div>
    </div>

    <div class="weekGrafiek grafiek" id="js--weekGrafiek" >
      <div class="graphContainer">
          <canvas id="js--canvasWeek"></canvas>
      </div>
    </div>

    <div class="maandGrafiek grafiek" id="js--maandGrafiek" >
      <div class="graphContainer">
          <canvas id="js--canvasMaand"></canvas>
      </div>
    </div>
</div>
@endsection

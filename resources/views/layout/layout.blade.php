<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{--<link href="{{ asset('css/style.css') }}" rel="stylesheet"> --}}
    {{-- <script src="{{ asset('js/side.js') }}" defer></script> --}}
    <script type="text/javascript" src="{{URL::asset('js/time.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/side.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.2.4/gsap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@500&display=swap" rel="stylesheet">

    <title>FruitApp</title>
</head>
<body>
    <img class ="background" src="img/sinaasappel.png" alt="">
    <img class ="background" src="img/banaan.png" alt="">
    <img class ="background" src="img/appel.png" alt="">
    <img class ="background" src="img/peer.png" alt="">
    <img class ="background" src="img/sinaasappel.png" alt="">
    <img class ="background" src="img/banaan.png" alt="">
    <img class ="background" src="img/appel.png" alt="">
  <main>
    <div class="container-fluid">
        @yield('content')
    </div>
  </main>
</body>

<!-- imports om notifications werkend te krijgen-->
@jquery
@toastr_js
@toastr_render
@toastr_css
</html>

//haal de data op
const fetchApi = (url) =>{
    return fetch(url).then((data) => {
        return data.json();
    })
}

// haal het canvas op uit het blade bestand
const ctx = document.getElementById("js--canvas").getContext('2d');

//pak de data die meegegeven wordt op pad getData
fetchApi('/getData').then((response)=>{
    let users = [];
    let aantal = [];

    //pak alle items uit de response mee en vul de array's met de data uit de response
    for (let i = 0; i<response.length; i++){
        users.push(response[i].gebruikersnaam);
        aantal.push(response[i].dagscore);
    }
    console.log(users);
    console.log(aantal);

    //maak de chart aan
    let myChart = new Chart(ctx, {
        type: 'bar',
        data:{
            //geef de labels data van de users hun gebruiksnaam mee.
            labels: users,
            datasets: [{
                label:'Aantal stukken fruit per persoon',
                //stopt in de data het getal van de dagscore
                data: aantal,
                backgroundColor: "rgb(0,0,40)"
            }]
        },
        options:{
            scales:{
                yAxes:[{
                    ticks:{
                        beginAtZero: true
                    }
                }]
            }
        }
    })


})

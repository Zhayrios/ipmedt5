import serial
import os

port0 = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=3.0)   #fruit dispenser rechts
port1 = serial.Serial("/dev/ttyUSB1", baudrate=9600, timeout=3.0)   #fruit dispenser links
#port2 = serial.Serial("/dev/ttyUSB2", baudrate=9600, timeout=3.0)   #reward dispenser
#port3 = serial.Serial("/dev/ttyUSB3", baudrate=9600, timeout=3.0)  #Was de vingerprint scanner, maar die werkt slechts heel af en toe

while True:
    rcv0 = port0.readline().strip()
    if(rcv0 == 'b'):
        os.system("python update.py")

    rcv1 = port1.readline().strip()
    if(rcv1 == 'b'):
        os.system("python update.py")

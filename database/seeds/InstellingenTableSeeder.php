<?php

use Illuminate\Database\Seeder;

class InstellingenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instellingen')->insert([
            'minaantalfruit' => 1,
            'maxaantalfruit'=> 3,
            'eindvandag'=> '20:00'
        ]);
    }
}

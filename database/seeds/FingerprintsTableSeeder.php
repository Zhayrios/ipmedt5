<?php

use Illuminate\Database\Seeder;

class FingerprintsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fingerprints')->insert([
            'Gebruikersnaam'=>'Peter',
        ]);
    }
}

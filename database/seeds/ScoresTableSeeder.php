<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ScoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scores')->insert([
            'dagscore'=> 10,
            'weekscore'=> 140,
            'maandscore'=> 11240,
            'jaarscore'=> 10324000,
            'beloningscore'=> 0,
            'streak' => 3,
            'gebruikersnaam'=> 'Peter',
        ]);
        DB::table('scores')->insert([
            'dagscore'=> 0,
            'weekscore'=> 0,
            'maandscore'=> 0,
            'jaarscore'=> 0,
            'beloningscore'=> 0,
            'streak' => 2,
            'gebruikersnaam'=> 'zhayrios',
        ]);
        DB::table('scores')->insert([
            'dagscore'=> 100,
            'weekscore'=> 150,
            'maandscore'=> 2300,
            'jaarscore'=> 55000,
            'beloningscore'=> 0,
            'streak' => 2,
            'gebruikersnaam'=> 'ADMIN',
        ]);
        DB::table('scores')->insert([
            'dagscore'=> 60,
            'weekscore'=> 530,
            'maandscore'=> 3450,
            'jaarscore'=> 345370,
            'beloningscore'=> 0,
            'streak' => 1,
            'gebruikersnaam'=> 'Burak',
        ]);
    }
}

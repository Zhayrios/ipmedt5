<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Peter',
            'email'=> 'test1@testmail.nl',
            'password'=> 'P',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 0,
            'aantaldagengebruik' => 0
        ]);
        DB::table('users')->insert([
            'id' => 2,
            'name' => 'zhayrios',
            'email'=> 'test2@testmail.nl',
            'password'=> 'z',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 4,
            'aantaldagengebruik' => 3
        ]);
        DB::table('users')->insert([
            'id' => 3,
            'name' => 'ADMIN',
            'email'=> 'test3@testmail.nl',
            'password'=> 'ADMIN',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 10,
            'aantaldagengebruik' => 5
        ]);
        DB::table('users')->insert([
            'id' => 4,
            'name' => 'Burak',
            'email'=> 'test4@testmail.nl',
            'password'=> 'B',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 4,
            'aantaldagengebruik' => 10
        ]);
    }
}

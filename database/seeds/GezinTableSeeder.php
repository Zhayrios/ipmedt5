<?php

use Illuminate\Database\Seeder;

class GezinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gezin')->insert([
            'gezinscode' => 'ADMIN',
            'gebruikersnaam' => 'Peter'
        ]);
    }
}

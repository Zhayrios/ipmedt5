<?php

use Illuminate\Database\Seeder;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('user')->insert([
            'id' => 1,
            'gebruikersnaam' => 'Peter',
            'wachtwoord'=> 'P',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 0,
            'aantaldagengebruik' => 0
        ]);
        DB::table('user')->insert([
            'id' => 2,
            'gebruikersnaam' => 'zhayrios',
            'wachtwoord'=> 'z',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 4,
            'aantaldagengebruik' => 3
        ]);
        DB::table('user')->insert([
            'id' => 3,
            'gebruikersnaam' => 'ADMIN',
            'wachtwoord'=> 'ADMIN',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 10,
            'aantaldagengebruik' => 5
        ]);
        DB::table('user')->insert([
            'id' => 4,
            'gebruikersnaam' => 'Burak',
            'wachtwoord'=> 'B',
            'groepcode' => 'Hallo',
            'stukkenfruitgegeten'=> 4,
            'aantaldagengebruik' => 10
        ]);

    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGezinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gezin', function (Blueprint $table) {
            $table->string('gebruikersnaam')->unique();
            $table->string('gezinscode')->unique();

            $table->foreign('gebruikersnaam')->references('name')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gezin', function($table){
            $table->dropForeign('gezin_gebruikersnaam_foreign');
        });
        Schema::dropIfExists('gezin');
    }
}

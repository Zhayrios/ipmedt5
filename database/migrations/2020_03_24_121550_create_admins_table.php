<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->string('gebruikersnaam')->unique();
            $table->boolean('isAdmin')->default(false);
            $table->foreign('gebruikersnaam')->references('name')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function($table){
            $table->dropForeign('admins_gebruikersnaam_foreign');
        });
        Schema::dropIfExists('admins');
    }
}

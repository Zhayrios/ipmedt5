<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFingerprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fingerprints', function (Blueprint $table) {
            $table->string('gebruikersnaam')->unique();
            $table->id('fingerprintid')->unique();

            $table->foreign('gebruikersnaam')->references('name')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fingerprints', function($table){
            $table->dropForeign('fingerprints_gebruikersnaam_foreign');
        });
        Schema::dropIfExists('fingerprints');
    }
}

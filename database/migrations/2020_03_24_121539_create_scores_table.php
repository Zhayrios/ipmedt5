<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->string('gebruikersnaam')->unique();
            $table->integer('dagscore')->default('0');
            $table->integer('weekscore')->default('0');
            $table->integer('maandscore')->default('0');
            $table->integer('jaarscore')->default('0');
            $table->integer('beloningscore')->default('0');
            $table->integer('streak')->default('0');

            $table->foreign('gebruikersnaam')->references('name')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scores', function($table){
            $table->dropForeign('scores_gebruikersnaam_foreign');
        });
        Schema::dropIfExists('scores');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GroepscodeTussenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tussentable', function (Blueprint $table) {
            $table->string('gebruikersnaam')->unique();
            $table->string('groepscode')->nullable();
            $table->foreign('gebruikersnaam')->references('name')->on('users');
            $table->foreign('groepscode')->references('gezinscode')->on('gezin');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tussentable', function($table){
            $table->dropForeign('tussentable_gebruikersnaam_foreign');
            $table->dropForeign('tussentable_groepscode_foreign');
        });
        Schema::dropIfExists('tussentable');
    }
}

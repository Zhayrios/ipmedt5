<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->integerIncrements('id')->unique();
            $table->string('gebruikersnaam')->unique();
            $table->string('wachtwoord');
            $table->string('groepcode')->nullable();
            $table->integer('stukkenfruitgegeten')->default(0);
            $table->integer('aantaldagengebruik')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
